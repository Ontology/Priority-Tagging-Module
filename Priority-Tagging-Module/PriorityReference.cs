﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priority_Tagging_Module
{
    public class PriorityReference
    {
        public string IdRef { get; set; }
        public string NameRef { get; set; }
        public PriorityItem PriorityItem { get; set; }
    }
}
