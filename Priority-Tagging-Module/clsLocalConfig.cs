﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using OntologyAppDBConnector;
using ImportExport_Module;
using OntologyClasses.BaseClasses;
using System.Reflection;
using OntologyClasses.Interfaces;
using System.Runtime.InteropServices;

namespace Priority_Tagging_Module
{
    public class clsLocalConfig : ILocalConfig
    {
    private const string cstrID_Ontology = "57d4af64dd774c29a6c796c897e8b29a";
    private ImportWorker objImport;

    public Globals Globals { get; set; }

    private clsOntologyItem objOItem_DevConfig = new clsOntologyItem();
    public clsOntologyItem OItem_BaseConfig { get; set; }

    private OntologyModDBConnector objDBLevel_Config1;
    private OntologyModDBConnector objDBLevel_Config2;

        
        // AttributeTypes
        public clsOntologyItem OItem_attributetype_red { get; set; }
        public clsOntologyItem OItem_attributetype_grün { get; set; }
        public clsOntologyItem OItem_attributetype_blau { get; set; }
        public clsOntologyItem OItem_attributetype_alpha { get; set; }


        // Objects
        public clsOntologyItem OItem_object_important { get; set; }
    public clsOntologyItem OItem_object_less_important { get; set; }
    public clsOntologyItem OItem_object_normal { get; set; }
    public clsOntologyItem OItem_object_not_important { get; set; }
    public clsOntologyItem OItem_object_very_important { get; set; }

        // Class
        public clsOntologyItem OItem_class_priority { get; set; }
        public clsOntologyItem OItem_class_colors { get; set; }

        // RelationTypes
        public clsOntologyItem OItem_relationtype_is_tagging { get; set; }
        public clsOntologyItem OItem_relationtype_contains { get; set; }
        public clsOntologyItem OItem_relationtype_belongs_to { get; set; }
        public clsOntologyItem OItem_relationtype_back { get; set; }
        public clsOntologyItem OItem_relationtype_fore { get; set; }



        private void get_Data_DevelopmentConfig()
    {
        var objORL_Ontology_To_OntolgyItems = new List<clsObjectRel> {new clsObjectRel {ID_Object = cstrID_Ontology,
                                                                                             ID_RelationType = Globals.RelationType_contains.GUID,
                                                                                             ID_Parent_Other = Globals.Class_OntologyItems.GUID}};

        var objOItem_Result = objDBLevel_Config1.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
        if (objOItem_Result.GUID == Globals.LState_Success.GUID)
        {
            if (objDBLevel_Config1.ObjectRels.Any())
            {

                objORL_Ontology_To_OntolgyItems = objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingAttribute.GUID
                }).ToList();

                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingClass.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingObject.GUID
                }));
                objORL_Ontology_To_OntolgyItems.AddRange(objDBLevel_Config1.ObjectRels.Select(oi => new clsObjectRel
                {
                    ID_Object = oi.ID_Other,
                    ID_RelationType = Globals.RelationType_belongingRelationType.GUID
                }));

                objOItem_Result = objDBLevel_Config2.GetDataObjectRel(objORL_Ontology_To_OntolgyItems, doIds: false);
                if (objOItem_Result.GUID == Globals.LState_Success.GUID)
                {
                    if (!objDBLevel_Config2.ObjectRels.Any())
                    {
                        throw new Exception("Config-Error");
                    }
                }
                else
                {
                    throw new Exception("Config-Error");
                }
            }
            else
            {
                throw new Exception("Config-Error");
            }

        }

    }

    public clsLocalConfig()
    {
        Globals = new Globals();
        set_DBConnection();
        get_Config();
    }

    public clsLocalConfig(Globals Globals)
    {
        this.Globals = Globals;
        set_DBConnection();
        get_Config();
    }

    private void set_DBConnection()
    {
        objDBLevel_Config1 = new OntologyModDBConnector(Globals);
        objDBLevel_Config2 = new OntologyModDBConnector(Globals);
        objImport = new ImportWorker(Globals);
    }

    private void get_Config()
    {
        try
        {
            get_Data_DevelopmentConfig();
            get_Config_AttributeTypes();
            get_Config_RelationTypes();
            get_Config_Classes();
            get_Config_Objects();
        }
        catch (Exception ex)
        {
            var objAssembly = Assembly.GetExecutingAssembly();
            AssemblyTitleAttribute[] objCustomAttributes = (AssemblyTitleAttribute[])objAssembly.GetCustomAttributes(typeof(AssemblyTitleAttribute), false);
            var strTitle = "Unbekannt";
            if (objCustomAttributes.Length == 1)
            {
                strTitle = objCustomAttributes.First().Title;
            }

            var objOItem_Result = objImport.ImportTemplates(objAssembly);
            if (objOItem_Result.GUID != Globals.LState_Error.GUID)
            {
                get_Data_DevelopmentConfig();
                get_Config_AttributeTypes();
                get_Config_RelationTypes();
                get_Config_Classes();
                get_Config_Objects();
            }
            else
            {
                throw new Exception("Config not importable");
            }
        }
    }

    private void get_Config_AttributeTypes()
    {
            var objOList_attributetype_red = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "attributetype_red".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                              select objRef).ToList();

            if (objOList_attributetype_red.Any())
            {
                OItem_attributetype_red = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_red.First().ID_Other,
                    Name = objOList_attributetype_red.First().Name_Other,
                    GUID_Parent = objOList_attributetype_red.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_grün = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attributetype_grün".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

            if (objOList_attributetype_grün.Any())
            {
                OItem_attributetype_grün = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_grün.First().ID_Other,
                    Name = objOList_attributetype_grün.First().Name_Other,
                    GUID_Parent = objOList_attributetype_grün.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_blau = (from objOItem in objDBLevel_Config1.ObjectRels
                                               where objOItem.ID_Object == cstrID_Ontology
                                               join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                               where objRef.Name_Object.ToLower() == "attributetype_blau".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                               select objRef).ToList();

            if (objOList_attributetype_blau.Any())
            {
                OItem_attributetype_blau = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_blau.First().ID_Other,
                    Name = objOList_attributetype_blau.First().Name_Other,
                    GUID_Parent = objOList_attributetype_blau.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_attributetype_alpha = (from objOItem in objDBLevel_Config1.ObjectRels
                                                where objOItem.ID_Object == cstrID_Ontology
                                                join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                where objRef.Name_Object.ToLower() == "attributetype_alpha".ToLower() && objRef.Ontology == Globals.Type_AttributeType
                                                select objRef).ToList();

            if (objOList_attributetype_alpha.Any())
            {
                OItem_attributetype_alpha = new clsOntologyItem()
                {
                    GUID = objOList_attributetype_alpha.First().ID_Other,
                    Name = objOList_attributetype_alpha.First().Name_Other,
                    GUID_Parent = objOList_attributetype_alpha.First().ID_Parent_Other,
                    Type = Globals.Type_AttributeType
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

    private void get_Config_RelationTypes()
    {
            var objOList_relationtype_back = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_back".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_back.Any())
            {
                OItem_relationtype_back = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_back.First().ID_Other,
                    Name = objOList_relationtype_back.First().Name_Other,
                    GUID_Parent = objOList_relationtype_back.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_fore = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "relationtype_fore".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                              select objRef).ToList();

            if (objOList_relationtype_fore.Any())
            {
                OItem_relationtype_fore = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_fore.First().ID_Other,
                    Name = objOList_relationtype_fore.First().Name_Other,
                    GUID_Parent = objOList_relationtype_fore.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_contains = (from objOItem in objDBLevel_Config1.ObjectRels
                                                  where objOItem.ID_Object == cstrID_Ontology
                                                  join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                  where objRef.Name_Object.ToLower() == "relationtype_contains".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                  select objRef).ToList();

            if (objOList_relationtype_contains.Any())
            {
                OItem_relationtype_contains = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_contains.First().ID_Other,
                    Name = objOList_relationtype_contains.First().Name_Other,
                    GUID_Parent = objOList_relationtype_contains.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_belongs_to = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_belongs_to".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_belongs_to.Any())
            {
                OItem_relationtype_belongs_to = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_belongs_to.First().ID_Other,
                    Name = objOList_relationtype_belongs_to.First().Name_Other,
                    GUID_Parent = objOList_relationtype_belongs_to.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_relationtype_is_tagging = (from objOItem in objDBLevel_Config1.ObjectRels
                                                    where objOItem.ID_Object == cstrID_Ontology
                                                    join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                                    where objRef.Name_Object.ToLower() == "relationtype_is_tagging".ToLower() && objRef.Ontology == Globals.Type_RelationType
                                                    select objRef).ToList();

            if (objOList_relationtype_is_tagging.Any())
            {
                OItem_relationtype_is_tagging = new clsOntologyItem()
                {
                    GUID = objOList_relationtype_is_tagging.First().ID_Other,
                    Name = objOList_relationtype_is_tagging.First().Name_Other,
                    GUID_Parent = objOList_relationtype_is_tagging.First().ID_Parent_Other,
                    Type = Globals.Type_RelationType
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

    private void get_Config_Objects()
    {
            var objOList_object_baseconfig = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_baseconfig".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

            if (objOList_object_baseconfig.Any())
            {
                OItem_BaseConfig = new clsOntologyItem()
                {
                    GUID = objOList_object_baseconfig.First().ID_Other,
                    Name = objOList_object_baseconfig.First().Name_Other,
                    GUID_Parent = objOList_object_baseconfig.First().ID_Parent_Other,
                    Type = Globals.Type_Object
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_object_important = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "object_important".ToLower() && objRef.Ontology == Globals.Type_Object
                                         select objRef).ToList();

        if (objOList_object_important.Any())
        {
            OItem_object_important = new clsOntologyItem()
            {
                GUID = objOList_object_important.First().ID_Other,
                Name = objOList_object_important.First().Name_Other,
                GUID_Parent = objOList_object_important.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_less_important = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_less_important".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_object_less_important.Any())
        {
            OItem_object_less_important = new clsOntologyItem()
            {
                GUID = objOList_object_less_important.First().ID_Other,
                Name = objOList_object_less_important.First().Name_Other,
                GUID_Parent = objOList_object_less_important.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_normal = (from objOItem in objDBLevel_Config1.ObjectRels
                                      where objOItem.ID_Object == cstrID_Ontology
                                      join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                      where objRef.Name_Object.ToLower() == "object_normal".ToLower() && objRef.Ontology == Globals.Type_Object
                                      select objRef).ToList();

        if (objOList_object_normal.Any())
        {
            OItem_object_normal = new clsOntologyItem()
            {
                GUID = objOList_object_normal.First().ID_Other,
                Name = objOList_object_normal.First().Name_Other,
                GUID_Parent = objOList_object_normal.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_not_important = (from objOItem in objDBLevel_Config1.ObjectRels
                                             where objOItem.ID_Object == cstrID_Ontology
                                             join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                             where objRef.Name_Object.ToLower() == "object_not_important".ToLower() && objRef.Ontology == Globals.Type_Object
                                             select objRef).ToList();

        if (objOList_object_not_important.Any())
        {
            OItem_object_not_important = new clsOntologyItem()
            {
                GUID = objOList_object_not_important.First().ID_Other,
                Name = objOList_object_not_important.First().Name_Other,
                GUID_Parent = objOList_object_not_important.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }

        var objOList_object_very_important = (from objOItem in objDBLevel_Config1.ObjectRels
                                              where objOItem.ID_Object == cstrID_Ontology
                                              join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                              where objRef.Name_Object.ToLower() == "object_very_important".ToLower() && objRef.Ontology == Globals.Type_Object
                                              select objRef).ToList();

        if (objOList_object_very_important.Any())
        {
            OItem_object_very_important = new clsOntologyItem()
            {
                GUID = objOList_object_very_important.First().ID_Other,
                Name = objOList_object_very_important.First().Name_Other,
                GUID_Parent = objOList_object_very_important.First().ID_Parent_Other,
                Type = Globals.Type_Object
            };
        }
        else
        {
            throw new Exception("config err");
        }


    }

    private void get_Config_Classes()
    {
            var objOList_class_colors = (from objOItem in objDBLevel_Config1.ObjectRels
                                         where objOItem.ID_Object == cstrID_Ontology
                                         join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                         where objRef.Name_Object.ToLower() == "class_colors".ToLower() && objRef.Ontology == Globals.Type_Class
                                         select objRef).ToList();

            if (objOList_class_colors.Any())
            {
                OItem_class_colors = new clsOntologyItem()
                {
                    GUID = objOList_class_colors.First().ID_Other,
                    Name = objOList_class_colors.First().Name_Other,
                    GUID_Parent = objOList_class_colors.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }

            var objOList_class_priority = (from objOItem in objDBLevel_Config1.ObjectRels
                                           where objOItem.ID_Object == cstrID_Ontology
                                           join objRef in objDBLevel_Config2.ObjectRels on objOItem.ID_Other equals objRef.ID_Object
                                           where objRef.Name_Object.ToLower() == "class_priority".ToLower() && objRef.Ontology == Globals.Type_Class
                                           select objRef).ToList();

            if (objOList_class_priority.Any())
            {
                OItem_class_priority = new clsOntologyItem()
                {
                    GUID = objOList_class_priority.First().ID_Other,
                    Name = objOList_class_priority.First().Name_Other,
                    GUID_Parent = objOList_class_priority.First().ID_Parent_Other,
                    Type = Globals.Type_Class
                };
            }
            else
            {
                throw new Exception("config err");
            }
        }

        public string IdLocalConfig
        {
            get
            {
                var attrib =
                      Assembly.GetExecutingAssembly()
                          .GetCustomAttributes(true)
                          .FirstOrDefault(objAttribute => objAttribute is GuidAttribute);
                if (attrib != null)
                {
                    return ((GuidAttribute)attrib).Value;
                }
                else
                {
                    return null;
                }
            }
        }

    }
    


}