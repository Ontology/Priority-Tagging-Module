﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Priority_Tagging_Module
{
    public class PriorityTaggingManager
    {
        public clsOntologyItem ResultBaseData { get; private set; }

        public List<PriorityItem> PriorityList { get; private set; }

        public PriorityItem ClearItem { get; private set; }

        private DataCollector_PriorityTagging dataCollector_PriorityTagging;

        public clsLocalConfig LocalConfig { get; private set; }

        private clsTransaction transactionManager;
        private clsRelationConfig relationManager;
        private OntologyModDBConnector dbWriterDelete;

        public ColorDefinition ColorDefinitionForm { get; private set; }

        public clsOntologyItem SetPriorityTag(clsOntologyItem oItemToTag, PriorityItem oPriority)
        {
            var result = LocalConfig.Globals.LState_Success.Clone();

            if (oItemToTag == null || oPriority == null)
            {
                return LocalConfig.Globals.LState_Error.Clone();
            }

            if (oPriority.OPriorityItem.GUID_Parent == null && oPriority.OPriorityItem.GUID != LocalConfig.OItem_BaseConfig.GUID)
            {
                return LocalConfig.Globals.LState_Error.Clone();
            }

            if (oPriority.OPriorityItem.GUID_Parent != LocalConfig.OItem_class_priority.GUID && oPriority.OPriorityItem.GUID != LocalConfig.OItem_BaseConfig.GUID)
            {
                return LocalConfig.Globals.LState_Error.Clone();
            }

            if (oPriority.OPriorityItem.GUID == LocalConfig.OItem_BaseConfig.GUID)
            {
                var delPriorities = new List<clsObjectRel>
                {
                    new clsObjectRel
                    {
                        ID_Other = oItemToTag.GUID,
                        ID_RelationType = LocalConfig.OItem_relationtype_is_tagging.GUID,
                        ID_Parent_Object = LocalConfig.OItem_class_priority.GUID
                    }
                };

                result = dbWriterDelete.DelObjectRels(delPriorities);

            }
            else
            {
                var savePriority = relationManager.Rel_ObjectRelation(oPriority.OPriorityItem, oItemToTag, LocalConfig.OItem_relationtype_is_tagging);
                transactionManager.ClearItems();
                result = transactionManager.do_Transaction(savePriority);
            }

            return result;

        }

        public List<PriorityReference> GetPriorityReferences(clsOntologyItem oItemRefItem = null, clsOntologyItem oItemPriority = null)
        {
            return dataCollector_PriorityTagging.GetData_PriorityReferences(oItemRefItem, oItemPriority);
        }

        public PriorityTaggingManager(Globals globals)
        {
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(globals);
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }
            
            Initialize();
        }

        public PriorityTaggingManager()
        {
            LocalConfig = (clsLocalConfig)LocalConfigManager.GetLocalConfig(((GuidAttribute)Assembly.GetExecutingAssembly().GetCustomAttributes(true).FirstOrDefault(objAttribute => objAttribute is GuidAttribute)).Value);
            if (LocalConfig == null)
            {
                LocalConfig = new clsLocalConfig(new Globals());
                LocalConfigManager.AddLocalConfig(LocalConfig);
            }

            Initialize();
        }

        private void Initialize()
        {
            dataCollector_PriorityTagging = new DataCollector_PriorityTagging(LocalConfig);
            dataCollector_PriorityTagging.PropertyChanged += DataCollector_PriorityTagging_PropertyChanged;
            transactionManager = new clsTransaction(LocalConfig.Globals);
            relationManager = new clsRelationConfig(LocalConfig.Globals);
            dbWriterDelete = new OntologyModDBConnector(LocalConfig.Globals);
            ResultBaseData = dataCollector_PriorityTagging.GetData_BaseDataToPrioritiesList();

            if (ResultBaseData.GUID == LocalConfig.Globals.LState_Success.GUID)
            {
                ColorDefinitionForm = new ColorDefinition(this);
            }
            

        }

        private void DataCollector_PriorityTagging_PropertyChanged(object sender, System.ComponentModel.PropertyChangedEventArgs e)
        {
            if (e.PropertyName == NotifyChanges.DataCollectorPriorityTagging_BaseDataToPriorityList)
            {
                PriorityList = dataCollector_PriorityTagging.BaseDataToPriorityList;


                ClearItem = new PriorityItem(new clsOntologyItem
                    {
                        GUID = LocalConfig.OItem_BaseConfig.GUID,
                        Name = ""
                    } 
                );

                PriorityList.Add(ClearItem);
            }
        }
    }
}
