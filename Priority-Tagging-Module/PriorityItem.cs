﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Priority_Tagging_Module.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priority_Tagging_Module
{
    public class PriorityItem : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;
        private OntologyModDBConnector dbReaderColor;
        private clsTransaction transactionManager;
        private clsRelationConfig relationManager;

        public string IdPriorityItem
        {
            get { return OPriorityItem != null ? OPriorityItem.GUID : null; }
        }
        public string NamePriorityItem
        {
            get { return OPriorityItem != null ? OPriorityItem.Name : null; }
        }

        private clsOntologyItem oPriorityItem;
        public clsOntologyItem OPriorityItem
        {
            get { return oPriorityItem; }
            private set
            {
                oPriorityItem = value;
            }
        }

        private ColorItem foreColorItem;
        public ColorItem ForeColorItem
        {
            get { return foreColorItem; }
            set
            {
                foreColorItem = value;

            }
        }

        private ColorItem backColorItem;
        public ColorItem BackColorItem
        {
            get { return backColorItem; }
            set
            {
                backColorItem = value;

            }
        }

        private clsOntologyItem result_Transaction;
        public clsOntologyItem Result_Transaction
        {
            get { return result_Transaction; }
            set
            {
                result_Transaction = value;
                RaisePropertyChanged(NotifyChanges.PriorityItem_Result_Transaction);
            }
        }

        public clsOntologyItem SetForeColor(Color foreColor)
        {
            var foreColorItem = new ColorItem(localConfig, foreColor.A, foreColor.R, foreColor.G, foreColor.B);
            if (foreColorItem.Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID || foreColorItem.Result_Format.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            ForeColorItem = foreColorItem;

            var saveColor = relationManager.Rel_ObjectRelation(OPriorityItem, foreColorItem.OColor, localConfig.OItem_relationtype_fore);

            transactionManager.ClearItems();
            return transactionManager.do_Transaction(saveColor, true);
        }

        public clsOntologyItem SetBackColor(Color backColor)
        {
            var backColorItem = new ColorItem(localConfig, backColor.A, backColor.R, backColor.G, backColor.B);
            if (backColorItem.Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID || backColorItem.Result_Format.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return localConfig.Globals.LState_Error.Clone();
            }

            BackColorItem = backColorItem;

            var saveColor = relationManager.Rel_ObjectRelation(OPriorityItem, backColorItem.OColor, localConfig.OItem_relationtype_back);

            transactionManager.ClearItems();
            return transactionManager.do_Transaction(saveColor, true);
        }


        public PriorityItem(clsOntologyItem oPriorityItem)
        {
            OPriorityItem = oPriorityItem;
        }

        public PriorityItem(clsLocalConfig localConfig, clsOntologyItem oPriorityItem)
        {
            this.localConfig = localConfig;
            OPriorityItem = oPriorityItem;

            Initialize();
        }

        public PriorityItem(clsLocalConfig localConfig, clsOntologyItem oPriorityItem, ColorItem foreColor, ColorItem backColor)
        {
            this.localConfig = localConfig;
            OPriorityItem = oPriorityItem;
            ForeColorItem = foreColor;
            BackColorItem = backColor;

            Initialize();
        }

        private void Initialize()
        {
            Result_Transaction = localConfig.Globals.LState_Success.Clone();
            transactionManager = new clsTransaction(localConfig.Globals);
            relationManager = new clsRelationConfig(localConfig.Globals);
            dbReaderColor = new OntologyModDBConnector(localConfig.Globals);
        }
        
    }
}
