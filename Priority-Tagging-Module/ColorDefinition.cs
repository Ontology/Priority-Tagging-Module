﻿using OntologyAppDBConnector;
using OntoMsg_Module;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Priority_Tagging_Module
{
    public partial class ColorDefinition : Form
    {
        private PriorityTaggingManager priorityTaggingManager;

        public ColorDefinition(PriorityTaggingManager priorityTaggingManager)
        {
            this.priorityTaggingManager = priorityTaggingManager;
            InitializeComponent();

            dataGridView_ColorDefinition.DataSource = null;
            if (this.priorityTaggingManager.ResultBaseData.GUID == priorityTaggingManager.LocalConfig.Globals.LState_Success.GUID)
            {
                dataGridView_ColorDefinition.DataSource = this.priorityTaggingManager.PriorityList.Where(prio => prio.OPriorityItem.GUID != priorityTaggingManager.LocalConfig.OItem_BaseConfig.GUID).ToList();
            }
            
        }

        
        private void toolStripButton_Close_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void dataGridView_ColorDefinition_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            
        }

        private void dataGridView_ColorDefinition_CellFormatting(object sender, DataGridViewCellFormattingEventArgs e)
        {
            if (dataGridView_ColorDefinition.Columns[e.ColumnIndex].DataPropertyName == NotifyChanges.PriorityItem_ForeColorItem)
            {
                var priorityItem = (PriorityItem)dataGridView_ColorDefinition.Rows[e.RowIndex].DataBoundItem;
                if (priorityItem.ForeColorItem != null)
                {
                    e.CellStyle.ForeColor = priorityItem.ForeColorItem.Color;
                    e.CellStyle.BackColor = priorityItem.ForeColorItem.Color;
                }

                
            }
            else if (dataGridView_ColorDefinition.Columns[e.ColumnIndex].DataPropertyName == NotifyChanges.PriorityItem_BackColorItem)
            {
                var priorityItem = (PriorityItem)dataGridView_ColorDefinition.Rows[e.RowIndex].DataBoundItem;
                if (priorityItem.BackColorItem != null)
                {
                    e.CellStyle.ForeColor = priorityItem.BackColorItem.Color;
                    e.CellStyle.BackColor = priorityItem.BackColorItem.Color;
                }

                
            }
        }

        private void dataGridView_ColorDefinition_CellMouseDoubleClick(object sender, DataGridViewCellMouseEventArgs e)
        {
            if (dataGridView_ColorDefinition.Columns[e.ColumnIndex].DataPropertyName == NotifyChanges.PriorityItem_ForeColorItem)
            {
                var priorityItem = (PriorityItem)dataGridView_ColorDefinition.Rows[e.RowIndex].DataBoundItem;
                if (priorityItem.ForeColorItem != null)
                {
                    colorDialog_Definition.Color = priorityItem.ForeColorItem.Color;
                }

                if (colorDialog_Definition.ShowDialog() == DialogResult.OK)
                {

                    var color = colorDialog_Definition.Color;
                    var result = priorityItem.SetForeColor(color);
                    if (result.GUID == priorityTaggingManager.LocalConfig.Globals.LState_Error.GUID)
                    {
                        MessageBox.Show(this, "Fehler beim Speichern der Farbe!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Close();
                    }
                    else
                    {
                        dataGridView_ColorDefinition.Invalidate();
                    }


                }

            }
            else if (dataGridView_ColorDefinition.Columns[e.ColumnIndex].DataPropertyName == NotifyChanges.PriorityItem_BackColorItem)
            {
                var priorityItem = (PriorityItem)dataGridView_ColorDefinition.Rows[e.RowIndex].DataBoundItem;
                if (priorityItem.BackColorItem != null)
                {
                    colorDialog_Definition.Color = priorityItem.BackColorItem.Color;
                }

                if (colorDialog_Definition.ShowDialog() == DialogResult.OK)
                {

                    var color = colorDialog_Definition.Color;
                    
                    var result = priorityItem.SetBackColor(color);
                    if (result.GUID == priorityTaggingManager.LocalConfig.Globals.LState_Error.GUID)
                    {
                        MessageBox.Show(this, "Fehler beim Speichern der Farbe!", "Fehler!", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        Close();
                    }
                    else
                    {
                        dataGridView_ColorDefinition.Invalidate();
                    }


                }
            }
        }
    }
}
