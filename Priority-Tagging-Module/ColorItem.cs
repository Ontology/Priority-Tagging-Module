﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Priority_Tagging_Module.BaseClasses;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priority_Tagging_Module
{
    public class ColorItem : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReaderColor;

        private clsTransaction transactionManager;
        private clsRelationConfig relationManager;

        private clsOntologyItem result_Transaction;
        public clsOntologyItem Result_Transaction
        {
            get { return result_Transaction; }
            set
            {
                result_Transaction = value;
                RaisePropertyChanged(NotifyChanges.ColorItem_Result_Transaction);
            }
        }

        private clsOntologyItem result_Format;
        public clsOntologyItem Result_Format
        {
            get { return result_Format; }
            set
            {
                result_Format = value;
                RaisePropertyChanged(NotifyChanges.ColorItem_Result_Format);
            }
        }

        public clsOntologyItem OColor { get; private set; }

        private clsObjectAtt oARed;
        private clsObjectAtt oAGreen;
        private clsObjectAtt oABlue;
        private clsObjectAtt oAAlpha;

        public Color Color { get; set; }
        
        public ColorItem(clsLocalConfig localConfig, clsOntologyItem oColor, clsObjectAtt oARed, clsObjectAtt oAGreen, clsObjectAtt oABlue, clsObjectAtt oAAlpha)
        {
            this.localConfig = localConfig;
            this.oARed = oARed;
            this.oAGreen = oAGreen;
            this.oABlue = oABlue;
            this.oAAlpha = oAAlpha;

            byte byteRed;
            byte byteGreen;
            byte byteBlue;
            byte byteAlpha;

            Result_Format = localConfig.Globals.LState_Success.Clone();
            if (!byte.TryParse(oARed.Val_Lng.ToString(), out byteRed)) Result_Format = localConfig.Globals.LState_Error.Clone();
            if (!byte.TryParse(oAGreen.Val_Lng.ToString(), out byteGreen)) Result_Format = localConfig.Globals.LState_Error.Clone();
            if (!byte.TryParse(oABlue.Val_Lng.ToString(), out byteBlue)) Result_Format = localConfig.Globals.LState_Error.Clone();
            if (!byte.TryParse(oAAlpha.Val_Lng.ToString(), out byteAlpha)) Result_Format = localConfig.Globals.LState_Error.Clone();

            if (Result_Format.GUID == localConfig.Globals.LState_Success.GUID)
            {
                Color = System.Drawing.Color.FromArgb(byteAlpha, byteRed, byteGreen, byteBlue);
            }

            Initialize();
        }

        public ColorItem(clsLocalConfig localConfig, byte alpha, byte red, byte green, byte blue)
        {
            this.localConfig = localConfig;
            Initialize();

            SaveColor(alpha, red, green, blue);
        }

        public void SaveColor(byte alpha, byte red, byte green, byte blue)
        {
            transactionManager.ClearItems();
           
            var searchColorItem = new List<clsObjectAtt>
            {
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_red.GUID,
                    ID_Class = localConfig.OItem_class_colors.GUID,
                    Val_Lng = red
                },
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_grün.GUID,
                    ID_Class = localConfig.OItem_class_colors.GUID,
                    Val_Lng = green
                },
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_blau.GUID,
                    ID_Class = localConfig.OItem_class_colors.GUID,
                    Val_Lng = blue
                },
                new clsObjectAtt
                {
                    ID_AttributeType = localConfig.OItem_attributetype_alpha.GUID,
                    ID_Class = localConfig.OItem_class_colors.GUID,
                    Val_Lng = alpha
                }
            };

            Result_Transaction = dbReaderColor.GetDataObjectAtt(searchColorItem);

            if (Result_Transaction.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var colorItem = (from redItem in dbReaderColor.ObjAtts.Where(colorAtt => colorAtt.ID_AttributeType == localConfig.OItem_attributetype_red.GUID)
                                join greenItem in dbReaderColor.ObjAtts.Where(colorAtt => colorAtt.ID_AttributeType == localConfig.OItem_attributetype_grün.GUID) on redItem.ID_Object equals greenItem.ID_Object
                                join blueItem in dbReaderColor.ObjAtts.Where(colorAtt => colorAtt.ID_AttributeType == localConfig.OItem_attributetype_blau.GUID) on redItem.ID_Object equals blueItem.ID_Object
                                join alphaItem in dbReaderColor.ObjAtts.Where(colorAtt => colorAtt.ID_AttributeType == localConfig.OItem_attributetype_alpha.GUID) on redItem.ID_Object equals alphaItem.ID_Object
                                where redItem.Val_Lng == red && greenItem.Val_Lng == green && blueItem.Val_Lng == blue && alphaItem.Val_Lng == alpha
                                select new 
                                {
                                    redItem, greenItem, blueItem, alphaItem
                                }).FirstOrDefault();

                if (colorItem != null)
                {
                    OColor = new clsOntologyItem
                    {
                        GUID = colorItem.redItem.ID_Object,
                        Name = colorItem.redItem.Name_Object,
                        GUID_Parent = colorItem.redItem.ID_Class,
                        Type = localConfig.Globals.Type_Object
                    };

                    oARed = colorItem.redItem;
                    oAGreen = colorItem.greenItem;
                    oABlue = colorItem.blueItem;
                    oAAlpha = colorItem.alphaItem;
                }
            }



           

            if (Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return;
            }

            if (OColor == null)
            {
                OColor = new clsOntologyItem
                {
                    GUID = localConfig.Globals.NewGUID,
                    Name = alpha.ToString() + "," + red.ToString() + "," + green.ToString() + "," + blue.ToString(),
                    GUID_Parent = localConfig.OItem_class_colors.GUID,
                    Type = localConfig.Globals.Type_Object
                };

                Result_Transaction = transactionManager.do_Transaction(OColor);

                if (Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transactionManager.rollback();
                    return;
                }

                var saveRed = relationManager.Rel_ObjectAttribute(OColor, localConfig.OItem_attributetype_red, (long)red);

                Result_Transaction = transactionManager.do_Transaction(saveRed);

                if (Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transactionManager.rollback();
                    return;
                }

                var saveGreen = relationManager.Rel_ObjectAttribute(OColor, localConfig.OItem_attributetype_grün, (long)green);

                Result_Transaction = transactionManager.do_Transaction(saveGreen);

                if (Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transactionManager.rollback();
                    return;
                }

                var saveBlue = relationManager.Rel_ObjectAttribute(OColor, localConfig.OItem_attributetype_blau, (long)blue);

                Result_Transaction = transactionManager.do_Transaction(saveBlue);

                if (Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transactionManager.rollback();
                    return;
                }

                var saveAlpha = relationManager.Rel_ObjectAttribute(OColor, localConfig.OItem_attributetype_alpha, (long)alpha);

                Result_Transaction = transactionManager.do_Transaction(saveAlpha);

                if (Result_Transaction.GUID == localConfig.Globals.LState_Error.GUID)
                {
                    transactionManager.rollback();
                }

            }


           


            

            
        }

        private void Initialize()
        {
            Result_Format = localConfig.Globals.LState_Success.Clone();
            Result_Transaction = localConfig.Globals.LState_Success.Clone();
            transactionManager = new clsTransaction(localConfig.Globals);
            relationManager = new clsRelationConfig(localConfig.Globals);
            dbReaderColor = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
