﻿using OntologyAppDBConnector;
using OntologyClasses.BaseClasses;
using Priority_Tagging_Module.BaseClasses;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Priority_Tagging_Module
{
    public class DataCollector_PriorityTagging : NotifyPropertyChange
    {
        private clsLocalConfig localConfig;

        private OntologyModDBConnector dbReader_Priorities;
        private OntologyModDBConnector dbReader_Colors;
        private OntologyModDBConnector dbReader_ColorAtts;
        private OntologyModDBConnector dbReader_PriorityRefs;

        private List<PriorityItem> baseDataToPriorityList;
        public List<PriorityItem> BaseDataToPriorityList
        {
            get { return baseDataToPriorityList; }
            private set
            {
                baseDataToPriorityList = value;
                RaisePropertyChanged(NotifyChanges.DataCollectorPriorityTagging_BaseDataToPriorityList);
            }
        }


        public clsOntologyItem GetData_BaseDataToPrioritiesList()
        {
            var searchPriorities = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = localConfig.OItem_BaseConfig.GUID,
                    ID_RelationType = localConfig.OItem_relationtype_contains.GUID,
                    ID_Parent_Other = localConfig.OItem_class_priority.GUID
                }
            };


            var result = dbReader_Priorities.GetDataObjectRel(searchPriorities);

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchColors = dbReader_Priorities.ObjectRels.Select(prio => new clsObjectRel
                {
                    ID_Object = prio.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_back.GUID,
                    ID_Parent_Other = localConfig.OItem_class_colors.GUID
                }).ToList();

                searchColors.AddRange(dbReader_Priorities.ObjectRels.Select(prio => new clsObjectRel
                {
                    ID_Object = prio.ID_Other,
                    ID_RelationType = localConfig.OItem_relationtype_fore.GUID,
                    ID_Parent_Other = localConfig.OItem_class_colors.GUID
                }));

                if (searchColors.Any())
                {
                    result = dbReader_Colors.GetDataObjectRel(searchColors);

                }
                else
                {
                    dbReader_Colors.ObjectRels.Clear();
                }
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var searchColorAtt = dbReader_Colors.ObjectRels.Select(col => new clsObjectAtt
                {
                    ID_Object = col.ID_Other,
                    ID_AttributeType = localConfig.OItem_attributetype_red.GUID
                }).ToList();

                searchColorAtt.AddRange(dbReader_Colors.ObjectRels.Select(col => new clsObjectAtt
                {
                    ID_Object = col.ID_Other,
                    ID_AttributeType = localConfig.OItem_attributetype_grün.GUID
                }));

                searchColorAtt.AddRange(dbReader_Colors.ObjectRels.Select(col => new clsObjectAtt
                {
                    ID_Object = col.ID_Other,
                    ID_AttributeType = localConfig.OItem_attributetype_blau.GUID
                }));

                searchColorAtt.AddRange(dbReader_Colors.ObjectRels.Select(col => new clsObjectAtt
                {
                    ID_Object = col.ID_Other,
                    ID_AttributeType = localConfig.OItem_attributetype_alpha.GUID
                }));

                if (searchColorAtt.Any())
                {
                    result = dbReader_ColorAtts.GetDataObjectAtt(searchColorAtt);
                }
                else
                {
                    dbReader_ColorAtts.ObjAtts.Clear();
                }
            }

            if (result.GUID == localConfig.Globals.LState_Success.GUID)
            {
                var foreColors = (from color in dbReader_Colors.ObjectRels.Where(col => col.ID_RelationType == localConfig.OItem_relationtype_fore.GUID)
                                  join redItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_red.GUID) on color.ID_Other equals redItem.ID_Object
                                  join greenItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_grün.GUID) on color.ID_Other equals greenItem.ID_Object
                                  join blueItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_blau.GUID) on color.ID_Other equals blueItem.ID_Object
                                  join alphaItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_alpha.GUID) on color.ID_Other equals alphaItem.ID_Object
                                  select new
                                  {
                                      ColorRel = color,
                                      ColorItem = new ColorItem(localConfig, new clsOntologyItem { GUID = color.ID_Other, Name = color.Name_Other, GUID_Parent = color.ID_Parent_Other, Type = localConfig.Globals.Type_Object }, redItem, greenItem, blueItem, alphaItem)

                                  });

                var backColors = (from color in dbReader_Colors.ObjectRels.Where(col => col.ID_RelationType == localConfig.OItem_relationtype_back.GUID)
                                  join redItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_red.GUID) on color.ID_Other equals redItem.ID_Object
                                  join greenItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_grün.GUID) on color.ID_Other equals greenItem.ID_Object
                                  join blueItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_blau.GUID) on color.ID_Other equals blueItem.ID_Object
                                  join alphaItem in dbReader_ColorAtts.ObjAtts.Where(colAtt => colAtt.ID_AttributeType == localConfig.OItem_attributetype_alpha.GUID) on color.ID_Other equals alphaItem.ID_Object
                                  select new
                                  {
                                      ColorRel = color,
                                      ColorItem = new ColorItem(localConfig, new clsOntologyItem { GUID = color.ID_Other, Name = color.Name_Other, GUID_Parent = color.ID_Parent_Other, Type = localConfig.Globals.Type_Object }, redItem, greenItem, blueItem, alphaItem)
                                  
                                  });

                BaseDataToPriorityList = (from prio in dbReader_Priorities.ObjectRels.OrderBy(prio => prio.OrderID)
                                          join foreColor in foreColors on prio.ID_Other equals foreColor.ColorRel.ID_Object into foreColors1
                                          from foreColor in foreColors1.DefaultIfEmpty()
                                          join backColor in backColors on prio.ID_Other equals backColor.ColorRel.ID_Object into backColors1
                                          from backColor in backColors1.DefaultIfEmpty()
                                          select new PriorityItem(localConfig, new clsOntologyItem { GUID = prio.ID_Other, Name = prio.Name_Other, GUID_Parent = prio.ID_Parent_Other, Type = localConfig.Globals.Type_Object }, foreColor != null ? foreColor.ColorItem : null, backColor != null ? backColor.ColorItem: null)).ToList();
            }

            return result;
        }



        public List<PriorityReference> GetData_PriorityReferences(clsOntologyItem oItemRefItem = null, clsOntologyItem oItemPriority = null)
        {
            List<clsObjectRel> searchPriorityRefs = null;

            
            searchPriorityRefs = new List<clsObjectRel>
            {
                new clsObjectRel
                {
                    ID_Object = oItemPriority != null ? oItemPriority.GUID : null,
                    ID_Parent_Object = oItemPriority != null ? oItemPriority.GUID_Parent : localConfig.OItem_class_priority.GUID,
                    ID_Other = oItemRefItem != null ? oItemRefItem.GUID : null,
                    ID_Parent_Other = oItemRefItem != null ? oItemRefItem.GUID_Parent : null,
                    ID_RelationType = localConfig.OItem_relationtype_is_tagging.GUID
                }
            };

            

            var result = dbReader_PriorityRefs.GetDataObjectRel(searchPriorityRefs, doIds:true);

            if (result.GUID == localConfig.Globals.LState_Error.GUID)
            {
                return null;
            }

            var resultItems = (from priorityBase in BaseDataToPriorityList
                               join priority in dbReader_PriorityRefs.ObjectRelsId on priorityBase.IdPriorityItem equals priority.ID_Object
                               select new PriorityReference { IdRef = priority.ID_Other, NameRef = priority.Name_Other, PriorityItem = priorityBase }).ToList();

            return resultItems;
        }

        public DataCollector_PriorityTagging(clsLocalConfig localConfig)
        {
            this.localConfig = localConfig;
            Initialize();
        }

        private void Initialize()
        {
            dbReader_Priorities = new OntologyModDBConnector(localConfig.Globals);
            dbReader_Colors = new OntologyModDBConnector(localConfig.Globals);
            dbReader_ColorAtts = new OntologyModDBConnector(localConfig.Globals);
            dbReader_PriorityRefs = new OntologyModDBConnector(localConfig.Globals);
        }
    }
}
